import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.util.KeywordUtil

Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nominal'), 20)

if(Mobile.verifyElementVisible(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 20, FailureHandling.OPTIONAL) == true) {
	
	KeywordUtil.markPassed('Check Box "Simpan Untuk selanjutnya" is Enable ')
	Mobile.comment('Check Box "Simpan Untuk selanjutnya" is Enable ')
	
} else {
	KeywordUtil.markPassed('Check Box "Simpan Untuk selanjutnya" is Disable')
	Mobile.comment('Check Box "Simpan Untuk selanjutnya" is Disable')
}
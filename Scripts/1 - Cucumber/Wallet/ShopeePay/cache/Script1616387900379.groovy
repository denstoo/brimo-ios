import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

GlobalVariable.username = 'brimosv004'

WebUI.callTestCase(findTestCase('General/Database Connect'), [:], FailureHandling.STOP_ON_FAILURE)

//String queryListPurchase = (((((((((((((((((('INSERT INTO tbl_list_purchase (username,purchase_number,purchase_nickname,status,purchase_type_id,group_id,favorite,F1) VALUES ' +
//'(\'') + GlobalVariable.username) + '\',\'085559547821\',\'LinkAja\',1,24,4,0,null),') + //24 LinkAja
//'(\'') + GlobalVariable.username) + '\',\'088218770335\',\'xyz 123 !@?\',1,24,4,0,null),') + //24 LinkAja
//'(\'') + GlobalVariable.username) + '\',\'082299888040\',\'DANA\',1,27,4,0,null),') + //27 DANA
//'(\'') + GlobalVariable.username) + '\',\'081288626368\',\'GoPay\',1,15,4,0,301342),') + //15 GoPay
//'(\'') + GlobalVariable.username) + '\',\'081290825284\',\'ShopeePay\',1,26,4,0,null),') + //26 ShopeePay
//'(\'') + GlobalVariable.username) + '\',\'081218022786\',\'OVO\',1,25,4,1,null),') + //25 OVO
//
//CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + GlobalVariable.username) +
//	'" and group_id = "4"')
//
//CustomKeywords.'database.methods.executeUpdate'(queryListPurchase)

//String tbl_history_purchase_ewallet = '''[
//{"number":"08008008002","name":"OVO","customer":"OVO TE** D**","code":"25","type":"25"},
//{"number":"085691335269","name":"LinkAja","customer":"FIKRI ARDIANSYAH","code":"24","type":"null"},
//{"number":"082299888040","name":"DANA","customer":"Syahrul Ramdhan","code":"27","type":"301341"},
//{"number":"085691335269","name":"LinkAja","customer":"FIKRI ARDIANSYAH","code":"24","type":"null"},
//{"number":"081212341234","name":"GoPay","customer":"Bakti Pratama","code":"15","type":"301341"},
//{"number":"08567641510","name":"ShopeePay","customer":"kozo112233","code":"26","type":"null"}
//]'''

String tbl_history_purchase_ewallet = '''[{"number":"08567641510","name":"ShopeePay","customer":"kozo112233","code":"26","type":"null"},{"number":"08008008002","name":"OVO","customer":"OVO TE** D**","code":"25","type":"25"},{"number":"085691335269","name":"LinkAja","customer":"FIKRI ARDIANSYAH","code":"24","type":"null"},{"number":"082299888040","name":"DANA","customer":"Syahrul Ramdhan","code":"27","type":"301341"},{"number":"085691335269","name":"LinkAja","customer":"FIKRI ARDIANSYAH","code":"24","type":"null"},{"number":"081212341234","name":"GoPay","customer":"Bakti Pratama","code":"15","type":"301341"}]'''

CustomKeywords.'database.methods.executeUpdate'(('UPDATE tbl_history_purchase_ewallet set value = "'+ tbl_history_purchase_ewallet +'" WHERE username = "' + GlobalVariable.username) + '"')

WebUI.callTestCase(findTestCase('General/Database Close'), [:], FailureHandling.STOP_ON_FAILURE)




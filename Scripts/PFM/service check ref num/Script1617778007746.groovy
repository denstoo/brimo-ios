import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver

Mobile.verifyElementExist(findTestObject('PFM/XCUIElementTypeStaticText - Catatan Keuangan Tittle'), 10)

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

List<WebElement> elements = driver.findElementsByClassName('XCUIElementTypeCell')

println('The size of elements is ::' + elements.size())

def list = []

for (WebElement cell : elements) {
	
	
	String index_cell = cell.getAttribute('index')
	
	if (index_cell == "true") {
		index_cell = "1"
		
	} else if (index_cell == "false") {
		index_cell = "0"
	}
	
	list.add(index_cell)
	
}

for(int i=0; i<=list.size() - 1; i++) {
	//Mobile.comment(list[i])
	
	indexTemp = list[i]
	
	Mobile.tap(findTestObject('PFM/list catatan pengeluaran/cek index/XCUIElementTypeCell', [('index'):"$indexTemp"]), 0)
	
	if(Mobile.verifyElementVisible(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - No Ref'), 10, FailureHandling.OPTIONAL) == true) {
		
		return null;
	} else {
		Mobile.tap(findTestObject('PFM/detail pengeluaran/XCUIElementTypeButton - Back'), 0)
	}
}

